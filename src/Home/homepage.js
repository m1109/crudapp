import {React,useEffect} from 'react';
import { useStateValue } from '../util/Stateprovider';
import './homepage.css';
const Home=()=>{
   const [,dispatch]=useStateValue();
   useEffect(()=>{
    document.title="Home";

    const loggedInUser = localStorage.getItem("user");
      if (loggedInUser) {
        const foundUser = JSON.parse(loggedInUser); 
        dispatch({
            type:'SETUSER',
            user:'hello',
            token:foundUser.token
        })
        
      }
   },[])
   
   return ( 
   <div className="homepage-style">
        <h1>Miracle Software Systems</h1>
    </div>
   );
}
export default Home;