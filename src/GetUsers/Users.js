import React from 'react';
import { useEffect,useState} from 'react';
import {Table} from 'react-bootstrap';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

import { useStateValue } from '../util/Stateprovider';
import Spinner from '../util/Spinner';

const Users =()=>{
    const [users,setUsers]=useState([]);
    const [{user,token},]=useStateValue();
    const [isLoading,setIsLoading]=useState(false);
    useEffect(()=>{
        function getusers()
        {  
            setIsLoading(true);
            const options={
                method:'GET',
                headers:{
                    'Content-Type':'Application/json',
                    'Authorization':token
                }
            }
            fetch('http://localhost:2614/user',options).
            then(async response=>{
                const data=await response.json();
                if(!response.ok)
                {
                    setIsLoading(false);
                }
                setUsers(data);
                console.log(data);
                setIsLoading(false);
            })
            .catch((error)=>
            {
                toast.error("Something went wrong");
                setIsLoading(false);
            });
        }
        getusers();
    },[])
    return(
        <React.Fragment> 
        <ToastContainer /> 
        {isLoading && <Spinner asOverlay />}
        {!isLoading &&
        <div>
            <Table striped bordered hover>
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Id</th>
    </tr>
  </thead>
  <tbody>
   {users.map((data)=>(
        <tr>
        
        <td>{data.DisplayName}</td>
        <td>{data.Email}</td>
        <td>{data.Id}</td>
      </tr> 
   )) }
    </tbody>
    </Table>
        </div> 
        }
        </React.Fragment>
    )
}
export default Users;