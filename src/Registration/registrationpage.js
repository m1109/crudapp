import React from "react";
import { useState,useEffect} from "react";
import "./registrationpage.css";
import { Card } from "react-bootstrap";
import { Button, Form } from "react-bootstrap"; 
import { useStateValue} from '../util/Stateprovider';
import { useHistory } from'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer,toast } from "react-toastify";
import Spinner from '../util/Spinner';
const Register = () => {
  useEffect(()=>{
    document.title="Register"
  },[]);
  let history=useHistory();
  const [,dispatch]=useStateValue();
  let [input1, setInput1] = useState([]);
  let [input2, setInput2] = useState([]);
  let [input3, setInput3] = useState([]);
  let [input4, setInput4] = useState([]);
  let [touched1, setTouched1] = useState(false);
  let [touched2, setTouched2] = useState(false);
  let [touched3, setTouched3] = useState(false);
  let [touched4, setTouched4] = useState(false);
  const [isLoading,setIsLoading]=useState(false);
  const setUserInput1 = (e) => {
    setInput1(e.target.value);
  };
  const setUserInput2 = (e) => {
    setInput2(e.target.value);
  };
  const setUserInput3 = (e) => {
    setInput3(e.target.value);
  };
  const setUserInput4 = (e) => {
    setInput4(e.target.value);
  };
  const setUserTouched1 = (e) => {
    setTouched1(true);
  };
  const setUserTouched2 = (e) => {
    setTouched2(true);
  };
  const setUserTouched3 = (e) => {
    setTouched3(true);
  };
  const setUserTouched4 = (e) => {
    setTouched4(true);
  };
  const handleSubmit23 = (e) => {
    if (input1.length <= 3) {
      console.log("please enter valid name");
    } else if (input2.length <= 3) {
      console.log("please enter email");
    } else if (input3.length !== 10) {
      console.log("please enter valid Phone Number");
    } else if (input4.length<= 1) {
      console.log("please enter password");
    } else {
      setIsLoading(true);
      const options = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ DisplayName: input1, Email: input2, Id:input3,Password:input4}),
      };
       fetch("http://localhost:2614/user/register", options)
        .then(async (response) => {
          const data = await response.json();
          if (!response.ok) {
            toast.error("Something Went wrong");
            setIsLoading(false);
          } else { 
            if(data.Registration=="success") 
            {
            dispatch({
              type:'SETUSER',
              user:input2,
              token:data.token
            });
            localStorage.setItem("user",JSON.stringify(data));
            setIsLoading(false);
            history.push("/");
          } 
          else 
          { 
            setIsLoading(false);
            toast.error("email or id already exits please try with another one")
          }
          }
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error("Something went wrong  server error");
        });
    }
  };

  return (
    <React.Fragment>
      <ToastContainer />
      {isLoading && <Spinner asOverlay />}
     {!isLoading &&  <div className="background-register-style">
      <div className="login-outer-style">
        <Card className="text-center">
          <Card.Header>Signup</Card.Header>
          <Form >
            <Form.Group className="mb-3 input-style" controlId="formBasicName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter name"
                onBlur={(e) => setUserTouched1(e)}
                onInput={(e) => setUserInput1(e)}
              />
              {input1.length <= 4 && touched1 && (
                <Form.Text className="text-muted">
                  <div className="touch-style"> Please enter a valid Name </div>
                </Form.Text>
              )}
            </Form.Group>
            <Form.Group
              className="mb-3 input-style1"
              controlId="formBasicEmail"
            >
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onBlur={(e) => setUserTouched2(e)}
                onInput={(e) => setUserInput2(e)}
              />
              {input2.length <= 4 && touched2 && (
                <Form.Text className="text-muted">
                  <div className="touch-style">
                    {" "}
                    Please enter a valid email address{" "}
                  </div>
                </Form.Text>
              )}
            </Form.Group>
            <Form.Group
              className="mb-3 input-style1"
              controlId="formBasicPhoneno"
            >
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Phone Number"
                onBlur={(e) => setUserTouched3(e)}
                onInput={(e) => setUserInput3(e)}
              />
              {input3.length !== 10 && touched3 && (
                <Form.Text className="text-muted">
                  <div className="touch-style">
                    Please enter a valid Phone Number
                  </div>
                </Form.Text>
              )}
            </Form.Group>
            <Form.Group
              className="mb-3 input-style1"
              controlId="formBasicPassword"
            >
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onBlur={(e) => setUserTouched4(e)}
                onInput={(e) => setUserInput4(e)}
              />
              {input4.length <= 3 && touched4 && (
                <Form.Text className="text-muted">
                  <div className="touch-style"> Please enter a password </div>
                </Form.Text>
              )}
            </Form.Group>
            <div className="input-button">
              <Button
                disabled={
                  input1.length <= 2 ||
                  input2.length <= 4 ||
                  input3.length !== 10 ||
                  input4.length <= 3
                }
                variant="primary"
                type="button"
                onClick={(e) => handleSubmit23(e)}
              >
                Submit
              </Button>
            </div>
          </Form>
        </Card>
      </div>
    </div> }
    </React.Fragment>
  );
};
export default Register;
